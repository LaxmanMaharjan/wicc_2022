def check_triangle():
    a,b,c = input("Enter length of 3 sides of Triangle seperated by space:").split()
    a,b,c = int(a),int(b),int(c)
    if (a+b) > c:
        if (a+c) > b:
            if (b+c) > a:
                print("Given length of 3 sides can form triangle.")
    else:
        print("Given length of 3 sides can not form a triangle.")

if __name__ == "__main__":
    check_triangle()
