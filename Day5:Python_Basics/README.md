# Day5: Python Basics

## Learnt about the fundamentals of python like:
1. math module
2. Functions
3. Conditional Statements
4. Operators
5. Recursion
6. Loops
7. Datatype
8. Debugging
