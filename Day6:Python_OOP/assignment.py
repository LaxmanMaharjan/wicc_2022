class Vehicle:
    model = None
    color = None
    no_of_wheels = None

    def horn(self):
        print("peep peep!")

    def change_tyre(self):
        print("The tyre has been changed.")

    def open_door(self):
        print('The door has been opened.')

    def close_door(self):
        print('The door has been closed.')

    def repair(self):
        print('The vehicle has been repaired.')

class Car(Vehicle):
    """
    It represents the Car class.
    """
    pass

class Bus(Vehicle):
    """
    It represents the Bus class.
    """
    def __init__(self,carrying_capacity=30, petrol_capacity=50, passenger_no = 0):
        self.carrying_capacity = carrying_capacity
        self.petrol_capacity = petrol_capacity
        self.passenger_no = passenger_no

    def add_passenger(self, num):
        # number of passenger after addition.
        passenger_sum = self.passenger_no + num
        if passenger_sum > self.carrying_capacity: 

            print(f"{self.carrying_capacity-self.passenger_no} passengers are added.\nNo empty seats left for {passenger_sum-self.carrying_capacity} passengers.")
            self.passenger_no = self.carrying_capacity
            
        else:
            self.passenger_no += num
            print(f'{num} passenger added successfully.')

    def remove_passenger(self,num):
        if self.passenger_no == 0:
            print('No passenger to remove.')
        else:
           self.passenger_no -= num
           print(f'{num} passengers removed successfully.')


