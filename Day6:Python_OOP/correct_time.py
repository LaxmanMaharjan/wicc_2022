class Time:
    """
    Represents the time of day.
    """
    def __init__(self, hour=0, minute=0, second=0):
        self.hour = hour
        self.minute = minute
        self.second = second

    def __add__(self, other):
        hr = self.hour + other.hour
        mins = self.minute + other.minute
        secs = self.second + other.second

        if secs > 59:
            secs = secs - 60
            mins += 1
        
        if mins > 59:
            mins = mins - 60
            hr += 1
        
        return Time(hr,mins,secs)

    def __str__(self):
        return f"Time({self.hour}:{self.minute}:{self.second})"

t1 = Time(1,35,35)
t2 = Time(1,35,35)
print(t1+t2)
